package com.mndlovu.grammaquiz;

public class QuizStructure {

    private final String question;
    private final String[] options;

    public QuizStructure(String question, String[] options) {
        this.question = question;
        this.options = options;
    }

    public String getQuestion() {
        return question;
    }

    public String[] getOptions() {
        return options;
    }
}
