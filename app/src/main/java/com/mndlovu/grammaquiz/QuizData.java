package com.mndlovu.grammaquiz;

public final class QuizData {
    public final static String PLACEHOLDER = "______";

    public final static String[] LIST_OF_QUESTIONS = {
            "I "+ PLACEHOLDER +" tennis every Sunday morning",
            "Steve "+ PLACEHOLDER +" his teeth before breakfast every morning",
            "Sorry, she can't come to the phone. She "+ PLACEHOLDER +" a bath",
            PLACEHOLDER +" many times every winter in FrankFurt.",
            "How many students in your class "+ PLACEHOLDER +" from USA?",
            "Babies "+ PLACEHOLDER +" when they are hungry.",
            "Jane "+ PLACEHOLDER +" her blue jeans today, but usually she wears " +
                    "a skirt or a dress.",
            "I think I "+ PLACEHOLDER +" a new calculator. This one does not work " +
                    "properly any more.",
            "Sorry, you can't borrow my pencil. I "+ PLACEHOLDER +" it myself.",
            "I "+ PLACEHOLDER +" my pen. Have you seen it?"
    };

    public final static String[][] LIST_OF_OPTIONS = {
            {"playing", "play", "am playing", "am play"},
            {"will cleaned", "is cleaning", "cleans", "clean"},
            {"is having", "having", "have", "has"},
            {"It snows", "It snowed", "It is snowing", "It is snow"},
            {"comes", "come", "came", "are coming"},
            {"cry", "cries", "cried", "are crying"},
            {"wears", "wearing", "wear", "is wearing"},
            {"needs", "needed", "need", "am needing"},
            {"was using", "using", "use", "am using"},
            {"will look", "looking", "look", "am looking"}
    };

    public final static String[] LIST_OF_CORRECT_ANSWERS = {
            "play",
            "cleans",
            "is having",
            "It snows",
            "come",
            "cry",
            "is wearing",
            "need",
            "am using",
            "am looking"
    };
}
