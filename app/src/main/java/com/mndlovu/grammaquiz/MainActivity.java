package com.mndlovu.grammaquiz;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView question;
    private Button optionOne;
    private Button optionTwo;
    private Button optionThree;
    private Button optionFour;
    private List<QuizStructure> quizList;
    private int i = 0;
    private int score = 0;
    private boolean correct = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewId();
        initQuiz();
        display(quizList.get(0));
    }

    /**
     * initialize quizList view
     */
    private void initViewId() {
        question = findViewById(R.id.question_text_view);
        optionOne = findViewById(R.id.option_one);
        optionTwo = findViewById(R.id.option_two);
        optionThree = findViewById(R.id.option_three);
        optionFour = findViewById(R.id.option_four);
    }

    /**
     * initialize quizList structure
     */
    private void initQuiz() {
        quizList = new ArrayList<>();
        for (int i = 0; i < QuizData.LIST_OF_QUESTIONS.length; i++) {
            quizList.add(new QuizStructure(
                    QuizData.LIST_OF_QUESTIONS[i],
                    QuizData.LIST_OF_OPTIONS[i])
            );
        }
    }

    /**
     * display the quizList
     *
     * @param quiz structure object
     */
    private void display(QuizStructure quiz) {
        question.setText(quiz.getQuestion());
        optionOne.setText(quiz.getOptions()[0]);
        optionTwo.setText(quiz.getOptions()[1]);
        optionThree.setText(quiz.getOptions()[2]);
        optionFour.setText(quiz.getOptions()[3]);
    }


    /**
     * get the pressed button value
     *
     * @param view
     */
    public void selectedOption(View view) {
        Button button = (Button) view;
        String s = button.getText().toString();
        String fullSentence = quizList.get(i).getQuestion().replace(QuizData.PLACEHOLDER, s.toUpperCase());
        question.setText(fullSentence);
        if (isCorrect(s)) {
            correct = true;
//            Toast.makeText(this, "true", Toast.LENGTH_SHORT).show();
        } else {
            correct = false;
//            Toast.makeText(this, "false", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * checks whether the string passed match the one in
     * the list of correct answers
     *
     * @param answer a selected answer
     * @return true if the answer matches or false if it doesn't
     */
    private boolean isCorrect(String answer) {
        if (QuizData.LIST_OF_CORRECT_ANSWERS[i].equals(answer)) {
            return true;
        }
        return false;
    }

    /**
     * submit the selected answer.
     *
     * @param view
     */
    public void submit(View view) {

        if (i == (QuizData.LIST_OF_OPTIONS.length - 1)) {
            setContentView(R.layout.results_activity);
            TextView t = findViewById(R.id.results_text_view);
            t.setText(score + " / 10");
            return;
        }
        if (correct) {
            score += 1;
        }
        i++;
        display(quizList.get(i));
    }

    /**
     * reset to default values.
     *
     * @param view
     */
    public void restartAQuiz(View view) {

        setContentView(R.layout.activity_main);
        initViewId();
        i = 0;
        score = 0;
        correct = false;
        display(quizList.get(0));
    }
}
